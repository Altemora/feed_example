import 'package:example_project/app/base/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AppToast {
  static FToast? _toastInstance;

  static FToast get _toast {
    _toastInstance ??= FToast();
    return _toastInstance!;
  }

  static init(BuildContext context) {
    _toast.init(context);
  }

  static show({
    required BuildContext? context,
    required String text,
    int seconds = 3,
  }) {
    if (context != null) _toast.init(context);

    _toast.showToast(
      child: _getToast(text: text),
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: seconds),
      positionedToastBuilder: (context, child) {
        return Align(
          alignment: Alignment.bottomCenter,
          child: Column(mainAxisSize: MainAxisSize.min, children: [child]),
        );
      },
    );
  }

  static Widget _getToast({required String text}) {
    return Container(
      margin: const EdgeInsets.only(left: 20, right: 20, bottom: 30),
      padding: const EdgeInsets.fromLTRB(15, 14, 20, 14),
      decoration: BoxDecoration(
        color: AppColors.gray30,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Text(
        text,
        style: const TextStyle(
          fontSize: 16,
          height: 1.5,
          color: AppColors.black,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}
