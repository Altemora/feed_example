abstract class AppUseCase<Repository, Result, Params> {
  final Repository repository;

  const AppUseCase(this.repository);

  Future<Result> execute([Params params]);
}
