import 'package:example_project/data/enums/app_genders.dart';

class User {
  final String name;
  final String lastname;
  final String avatarUrl;
  final Gender gender;

  String get fullName => '$name $lastname';

  const User({
    required this.name,
    required this.lastname,
    this.avatarUrl = '',
    this.gender = Gender.other,
  });
}
