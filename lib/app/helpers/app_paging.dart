import 'package:example_project/data/entities/app_status_object.dart';
import 'package:example_project/data/enums/app_status.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'dev.dart';

class PagingOutOfRange {
  @override
  String toString() {
    return 'PagingOutOfRange: trying to call addPage(start:true) when previousPage == 0';
  }
}

///Paging [items] data and other states
class PagingState<T> {
  final List<T> items;
  final int prevPage;
  final int nextPage;
  final bool isAllPagesLoading;
  final AppStatus status;
  final String message;

  bool get isPageLoading => status == AppStatus.waiting;

  bool get hasItems => items.isNotEmpty;

  bool get hasNotItems => items.isEmpty;

  PagingState({
    List<T>? items,
    this.isAllPagesLoading = false,
    this.prevPage = 0,
    this.nextPage = 1,
    this.message = '',
    this.status = AppStatus.waiting,
  }) : items = items ?? <T>[];

  PagingState<T> change({
    List<T>? items,
    bool? isAllPagesLoading,
    int? prevPage,
    int? nextPage,
    String? message,
    AppStatus? status,
  }) =>
      PagingState<T>(
        items: items ?? this.items,
        isAllPagesLoading: isAllPagesLoading ?? this.isAllPagesLoading,
        prevPage: prevPage ?? this.prevPage,
        nextPage: nextPage ?? this.nextPage,
        message: message ?? this.message,
        status: status ?? this.status,
      );
}

abstract class PageLoader<T> {
  void loadPage(int page);
}

abstract class PagingDelegate<T> {
  void onAllPagesLoaded();

  void onPageLoading(int page);

  void onPageLoaded(int page, List<T> data);

  void onPageError(int page, AppError error);
}

///Cubit for paging [state] (items and loading status)
abstract class PagingCubit<T> extends Cubit<PagingState<T>>
    implements PageLoader<T>, PagingDelegate<T> {
  Function(dynamic, [dynamic])? onCubitError;

  PagingCubit([this.onCubitError]) : super(PagingState<T>());

  void _onCubitError(e, [stack]) {
    if (onCubitError != null) {
      onCubitError!(e, stack);
    } else {
      Dev.error('onCubitError', e, stack);
    }
  }

  setError({AppStatus status = AppStatus.unknown, String message = ''}) {
    try {
      emit(state.change(
        message: message,
        status: status,
      ));
    } catch (e, stack) {
      Dev.error('onCubitError', e, stack);
    }
  }

  resetPage() {
    try {
      emit(state.change(
        nextPage: 1,
        prevPage: 0,
        items: <T>[],
        isAllPagesLoading: false,
        status: AppStatus.waiting,
      ));
    } catch (e, stack) {
      _onCubitError(e, stack);
    }
  }

  ///Set new items and set page to [page]
  setPage(List<T> newItems, {int page = 1}) {
    List<T> inItems = state.items;
    int inPage = page + 1;
    int inStartPage = state.prevPage;
    inItems.insertAll(0, newItems);
    Dev.log(
      'addPageWithPage({prevPage:$inStartPage, page:$inPage}): $inItems',
    );
    try {
      emit(state.change(
        nextPage: inPage,
        prevPage: inStartPage,
        items: inItems,
        status: AppStatus.success,
      ));
    } catch (e, stack) {
      _onCubitError(e, stack);
    }
  }

  ///Add new page to end or start if [start] is true
  addPage(List<T> newItems, {bool start = false}) {
    List<T> inItems = state.items;
    int inPage = state.nextPage;
    int inStartPage = state.prevPage;
    if (newItems.isNotEmpty) {
      if (start) {
        inItems.insertAll(0, newItems);
        inStartPage--;
      } else {
        inItems.addAll(newItems);
        inPage++;
      }
    }
    if (inStartPage < 0) throw PagingOutOfRange();
    try {
      emit(
        state.change(
          nextPage: inPage,
          prevPage: inStartPage,
          items: inItems,
          status: AppStatus.success,
        ),
      );
    } catch (e, stack) {
      _onCubitError(e, stack);
    }
  }

  _startLoading() {
    try {
      emit(state.change(
        status: AppStatus.waiting,
      ));
    } catch (e, stack) {
      _onCubitError(e, stack);
    }
  }

  _allLoaded() {
    try {
      emit(state.change(
        isAllPagesLoading: true,
        status: AppStatus.complete,
      ));
    } catch (e, stack) {
      _onCubitError(e, stack);
    }
  }

  ///Call to starting load pages from first page
  void loadStartPage() {
    resetPage();
    if (state.hasNotItems) loadPage(state.nextPage);
  }

  ///Call to starting with data from [n] page
  void loadStartWithDataAndPage(
      {required List<T> newItems, required int page}) {
    resetPage();
    onPageLoading(page);

    if (state.hasNotItems) setPage(newItems, page: page);
  }

  void loadNext() {
    loadPage(state.nextPage);
  }

  void loadPrev() {
    loadPage(state.prevPage);
  }

  ///Add to ScrollController listener or set to [AppScrollController] to onScroll parameter
  void onPagingScroll(ScrollController scrollController) {
    if (_checkLoadNext(scrollController)) {
      loadNext();
    } else if (_checkLoadPrev(scrollController)) {
      loadPrev();
    }
  }

  ///trigger when change page on [CarouselSlider]
  ///set to [CarouselOptions] to onPageChanged parameter
  void onPagingSwipeLast(indexSlide, allSlide) {
    if (_checkSlideLoadNext(indexSlide, allSlide)) {
      loadNext();
    } else if (_checkSlideLoadPrev(indexSlide)) {
      loadPrev();
    }
  }

  ///PagingDelegate realization
  ///You can @override some of this method, but don't forgot calling [super]
  @override
  void onAllPagesLoaded() {
    _allLoaded();
  }

  @override
  void onPageLoading(int page) {
    _startLoading();
  }

  @override
  void onPageLoaded(int page, List<T> data) {
    addPage(data);
  }

  @override
  void onPageError(int page, AppError error) {
    setError(status: error.status, message: error.message);
  }

  ///Calling in scroll controller listener
  bool _checkLoadNext(ScrollController scrollController) {
    return scrollController.offset >=
            scrollController.position.maxScrollExtent &&
        !scrollController.position.outOfRange &&
        !state.isPageLoading &&
        !state.isAllPagesLoading;
  }

  ///TODO: Create method to load previous page
  bool _checkLoadPrev(ScrollController scrollController) {
    /*scrollController.offset >= scrollController.position.maxScrollExtent &&
        !scrollController.position.outOfRange &&
        !state.isPageLoading &&
        !state.isAllPagesLoading;*/
    return false;
  }

  ///Calling in swipe slider controller listener
  bool _checkSlideLoadNext(indexSlide, allSlide) {
    Dev.log(state.nextPage);
    return indexSlide == allSlide && !state.isAllPagesLoading;
  }

  ///Calling in swipe slider controller listener
  bool _checkSlideLoadPrev(indexSlide) {
    Dev.log(state.nextPage);
    return indexSlide == 0 && !state.isAllPagesLoading;
  }
}
