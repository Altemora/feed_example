import 'package:example_project/data/entities/user.dart';
import 'package:example_project/data/repository/users_repository.dart';

import 'app_use_case.dart';

class GetUsersListCase extends AppUseCase<UsersRepository, List<User>, int> {
  const GetUsersListCase(UsersRepository repository) : super(repository);

  @override
  Future<List<User>> execute([int params = 0]) {
    return repository.getUsers(page: params);
  }
}
