import 'package:example_project/app/base/app_colors.dart';
import 'package:example_project/data/enums/app_genders.dart';
import 'package:flutter/material.dart';

class AppGenderLine extends StatelessWidget {
  final Gender gender;
  final double size;
  final Axis axis;

  const AppGenderLine({
    Key? key,
    required this.gender,
    required this.size,
    required this.axis,
  }) : super(key: key);

  Color get colorFromGender {
    switch (gender) {
      case Gender.male:
        return AppColors.blue;
      case Gender.female:
        return AppColors.pink;
      case Gender.other:
        return AppColors.yellow;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: axis == Axis.horizontal ? double.infinity : size,
      height: axis == Axis.vertical ? double.infinity : size,
      color: colorFromGender,
    );
  }
}
