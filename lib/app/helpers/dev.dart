import 'dart:developer' as dev;

class Dev {
  static void log(message) {
    dev.log('$message', level: 100, name: 'Info');
  }

  static void apiResponse(response) {
    dev.log('$response', level: 50, name: 'API');
  }

  static void error([String tag = 'UnknownError', error, StackTrace? trace]) {
    dev.log(tag, name: 'Error', error: '$error', stackTrace: trace);
    if (trace != null) {
      print(trace);
    }
  }
}
