import 'package:example_project/data/enums/app_status.dart';

const _unknown = 'unknown';

abstract class StatusObject<T extends Object?> {
  AppStatus get status;

  T? get data;
}

class AppResult<T extends Object> implements StatusObject<T> {
  final String tag;
  @override
  final AppStatus status;
  @override
  final T data;

  AppResult({
    this.tag = _unknown,
    this.status = AppStatus.success,
    required this.data,
  });
}

class AppError<T extends Object?> implements StatusObject<T> {
  final String tag;
  final String message;
  @override
  final AppStatus status;
  @override
  final T? data;

  AppError({
    this.message = 'Unknown error',
    this.status = AppStatus.unknown,
    this.tag = _unknown,
    this.data,
  });

  static AppError from<T>(
    Object? error, {
    T? data,
  }) {
    if (error is AppError) return error;
    return AppError<T>(
      status: AppStatus.unknownInternalError,
      message: 'Unknown internal error',
    );
  }

  @override
  String toString() {
    return 'Error [$status] in [$tag]: $message${data != null ? '\nData: $data' : ''}';
  }
}
