import 'package:flutter/widgets.dart';

class AppScrollController extends ScrollBaseController {
  AppScrollController({
    Function(ScrollController)? onScroll,
    bool useNotification = false,
  }) : super(useNotifications: useNotification, scrollCallback: onScroll);

  @override
  bool onScroll(ScrollNotification? notification) {
    return true;
  }
}

abstract class ScrollBaseController extends TrackingScrollController {
  final bool useNotifications;
  final Function(ScrollController)? scrollCallback;

  double get velocity => position.activity?.velocity ?? 0;

  double get sensitive => velocity.abs();

  ScrollBaseController({this.useNotifications = false, this.scrollCallback}) {
    if (!useNotifications || scrollCallback != null) {
      addListener(() => onScrollListener(null));
    }
  }

  @override
  dispose() {
    if (!useNotifications) removeListener(() => onScrollListener(null));
    super.dispose();
  }

  bool onScroll(ScrollNotification? notification);

  bool onScrollListener(ScrollNotification? notification) {
    bool result = true;
    if (scrollCallback != null) scrollCallback!(this);
    if (!useNotifications) result = onScroll(null);
    return result;
  }
}
