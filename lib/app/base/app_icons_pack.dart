/// Flutter icons IconsPackage
/// Copyright (C) 2022 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// * Material Design Icons, Copyright (C) Google, Inc
///         Author:    Google
///         License:   Apache 2.0 (https://www.apache.org/licenses/LICENSE-2.0)
///         Homepage:  https://design.google.com/icons/
///
// ignore_for_file: constant_identifier_names
import 'package:flutter/widgets.dart';

class AppIcons {
  AppIcons._();

  static const _kFontFam = 'IconsPackage';
  static const String? _kFontPkg = null;

  static const IconData clear = IconData(0xe800, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData dashboard = IconData(0xe801, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData done = IconData(0xe802, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData done_all = IconData(0xe803, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData chevron_left = IconData(0xe804, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData chevron_right = IconData(0xe805, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData expand_less = IconData(0xe806, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData expand_more = IconData(0xe807, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData more_vert = IconData(0xe808, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData more_horiz = IconData(0xe809, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData menu = IconData(0xe80a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData place = IconData(0xe80b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData visibility_off = IconData(0xe80c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData visibility = IconData(0xe80d, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData insert_photo = IconData(0xe80e, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData edit = IconData(0xe80f, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData person = IconData(0xe810, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
