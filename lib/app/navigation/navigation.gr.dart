// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i2;
import 'package:flutter/material.dart' as _i3;

import '../pages/feed_page/feed_page.dart' as _i1;
import 'navigation.dart' as _i4;

class AppRouter extends _i2.RootStackRouter {
  AppRouter([_i3.GlobalKey<_i3.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i2.PageFactory> pagesMap = {
    FeedRoute.name: (routeData) {
      return _i2.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i1.FeedPage(),
          transitionsBuilder: _i4.noneTransitions,
          opaque: true,
          barrierDismissible: false);
    }
  };

  @override
  List<_i2.RouteConfig> get routes => [
        _i2.RouteConfig('/#redirect',
            path: '/', redirectTo: '/feed', fullMatch: true),
        _i2.RouteConfig(FeedRoute.name, path: '/feed')
      ];
}

/// generated route for
/// [_i1.FeedPage]
class FeedRoute extends _i2.PageRouteInfo<void> {
  const FeedRoute() : super(FeedRoute.name, path: '/feed');

  static const String name = 'FeedRoute';
}
