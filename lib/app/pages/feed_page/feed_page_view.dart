import 'package:example_project/app/base/app_scroll_controller.dart';
import 'package:example_project/app/components/feed_widget.dart';
import 'package:example_project/app/helpers/app_paging.dart';
import 'package:example_project/app/widgets/app_loading.dart';
import 'package:example_project/app/widgets/app_toast.dart';
import 'package:example_project/data/entities/user.dart';
import 'package:example_project/data/enums/app_status.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'feed_cubit.dart';

class FeedPageView extends StatelessWidget {
  const FeedPageView({Key? key}) : super(key: key);

  Widget _feedWidget(User user) {
    return FeedWidget(
      fullName: user.fullName,
      gender: user.gender,
      url: user.avatarUrl,
    );
  }

  @override
  Widget build(BuildContext context) {
    FeedCubit cubit = context.read<FeedCubit>();

    ///Need to review this solution. Not the best...
    if (cubit.state.hasNotItems && !cubit.state.isAllPagesLoading) {
      context.read<FeedCubit>().loadStartPage();
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Peoples'),
      ),
      body: BlocConsumer<FeedCubit, PagingState<User>>(
        listenWhen: (previous, current) => current.status.isError,
        listener: (context, state) {
          AppToast.show(context: context, text: state.message);
        },
        builder: (context, state) {
          return ListView.builder(
            controller: AppScrollController(
              onScroll: context.read<FeedCubit>().onPagingScroll,
            ),
            itemCount: state.items.length,
            itemBuilder: (BuildContext context, int index) {
              if (index == state.items.length - 1 &&
                  state.status.isNotComplete) {
                return Column(
                  children: [
                    _feedWidget(state.items[index]),
                    const AppLoading(),
                  ],
                );
              }
              return _feedWidget(state.items[index]);
            },
          );
        },
      ),
    );
  }
}
