import 'package:example_project/app/base/app_colors.dart';
import 'package:example_project/app/base/app_icons_pack.dart';
import 'package:flutter/widgets.dart';

class AppPhoto extends StatelessWidget {
  final String url;
  final double? height;
  final double? width;

  const AppPhoto({Key? key, required this.url, this.height, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (url.isNotEmpty) {
      return SizedBox(
        height: height,
        width: width,
        child: Image(
          height: height,
          width: width,
          alignment: Alignment.center,
          fit: BoxFit.cover,
          image: Image.network(url).image,
        ),
      );
    } else {
      return Container(
        height: height,
        width: width,
        color: AppColors.grey,
        alignment: Alignment.center,
        child: const Icon(
          AppIcons.insert_photo,
          color: AppColors.white,
        ),
      );
    }
  }
}
