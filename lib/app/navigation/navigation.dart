import 'package:auto_route/annotations.dart';
import 'package:example_project/app/pages/feed_page/feed_page.dart';
import 'package:flutter/widgets.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: routes,
)
class $AppRouter {}

const routes = <AutoRoute>[
  CustomRoute(
    initial: true,
    page: FeedPage,
    path: '/feed',
    transitionsBuilder: noneTransitions,
  ),
];

///Empty route transition
Widget noneTransitions(
  BuildContext context,
  Animation<double> animation,
  Animation<double> secondaryAnimation,
  Widget child,
) =>
    child;
