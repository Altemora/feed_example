import 'package:flutter/material.dart';

import 'app/base/app_colors.dart';
import 'app/navigation/navigation.gr.dart';

/*
flutter pub run build_runner build --delete-conflicting-outputs
*/

final _appRouter = AppRouter();

void main() {
  runApp(
    const MyApp(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Example App',
      theme: ThemeData(
        primaryColor: AppColors.darkGray,
        primarySwatch: AppMaterialColors.dark,
        backgroundColor: AppColors.white,
      ),
      color: AppColors.milk,
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
    );
  }
}
