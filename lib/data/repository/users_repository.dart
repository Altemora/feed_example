import 'package:example_project/data/api/api_entities/api_user.dart';
import 'package:example_project/data/api/randomuser_api.dart';
import 'package:example_project/data/entities/user.dart';
import 'package:example_project/data/enums/app_genders.dart';

abstract class UsersRepository {
  Future<List<User>> getUsers({required int page});
}

class RandomUsersRepository implements UsersRepository {
  const RandomUsersRepository();

  @override
  Future<List<User>> getUsers({required int page}) async {
    List<ApiUser> users = await RandomuserApi.instance.getUsers(page);

    ///Get users from API
    return <User>[
      for (ApiUser user in users)
        User(
          avatarUrl: user.picture?.large ?? '',
          name: user.name?.first ?? '',
          lastname: user.name?.last ?? '',
          gender: user.gender == 'male'
              ? Gender.male
              : (user.gender == 'female' ? Gender.female : Gender.other),
        )
    ];
  }
}
