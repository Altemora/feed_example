extension StatusGroup on AppStatus {
  bool get isNotComplete => this != AppStatus.complete;

  bool get isWaiting => this == AppStatus.waiting;

  bool get isError {
    switch (this) {
      case AppStatus.unknown:
      case AppStatus.waiting:
      case AppStatus.success:
      case AppStatus.complete:
        return false;
      default:
        return true;
    }
  }
}

enum AppStatus {
  unknown,
  waiting,
  success,
  complete,
  notFound,
  connectionError,
  unknownInternalError,
  unknownOutsideError,
  dataRecognitionError,
}
