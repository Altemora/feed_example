import 'dart:math';

import 'package:example_project/app/helpers/app_paging.dart';
import 'package:example_project/data/entities/app_status_object.dart';
import 'package:example_project/data/entities/user.dart';
import 'package:example_project/data/enums/app_status.dart';
import 'package:example_project/data/repository/users_repository.dart';
import 'package:example_project/domain/use_cases/get_users_list_case.dart';

import 'package:freezed_annotation/freezed_annotation.dart';

part 'feed_cubit.freezed.dart';

@freezed
class FeedState with _$FeedState {
  const factory FeedState({
    required List<User> users,
    required int counter,
  }) = _FeedState;
}

class FeedCubit extends PagingCubit<User> {
  GetUsersListCase getUsersListCase = const GetUsersListCase(
    RandomUsersRepository(),
  );

  FeedCubit() : super();

  @override
  void loadPage(int page) async {
    try {
      onPageLoading(page);
      List<User> users = await getUsersListCase.execute(page);
      onPageLoaded(page, users);
    } catch (e) {
      if (e is AppError && e.status == AppStatus.complete) {
        onAllPagesLoaded();
      } else {
        onPageError(page, AppError.from(e));
      }
    }
  }
}
