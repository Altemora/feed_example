import 'package:dio/dio.dart';
import 'package:example_project/app/helpers/dev.dart';
import 'package:example_project/data/api/api_entities/api_user.dart';
import 'package:example_project/data/entities/app_status_object.dart';
import 'package:example_project/data/enums/app_status.dart';

class RandomuserApi {
  static const url = 'https://randomuser.me/api/';

  ///Api object instances
  static RandomuserApi get instance => const RandomuserApi._();

  const RandomuserApi._();

  Future<List<ApiUser>> getUsers(
    int page, {
    int results = 10,
    String seed = 'random',
  }) async {
    List<ApiUser> apiUsers = [];
    Response<dynamic>? response;

    ///Request
    try {
      response = await Dio().get(
        '$url?page=$page&results=$results&seed=$seed',
      );
    } catch (e) {
      throw AppError(
        status: AppStatus.unknownOutsideError,
        message: 'Oops, something wrong with API',
        data: e,
      );
    }

    ///Parse objects
    try {
      List<dynamic> apiUsersMap = response.data!['results'];
      for (var user in apiUsersMap) {
        apiUsers.add(ApiUser.fromJson(user));
      }
    } catch (e, trace) {
      Dev.error('Parse users', e, trace);
      throw AppError(
        status: AppStatus.dataRecognitionError,
        message: 'We do not understand API :(',
        data: e,
      );
    }

    ///Empty exception
    if (apiUsers.isEmpty) {
      throw AppError(
        status: AppStatus.complete,
        message: 'All pages of users was loaded!!! may be...',
      );
    }
    return apiUsers;
  }
}
