import 'package:example_project/app/base/app_colors.dart';
import 'package:example_project/app/base/app_icons_pack.dart';
import 'package:example_project/app/widgets/app_gender_line.dart';
import 'package:example_project/app/widgets/app_photo.dart';
import 'package:example_project/data/enums/app_genders.dart';
import 'package:flutter/material.dart';

class FeedWidget extends StatelessWidget {
  final String url;
  final String fullName;
  final Gender gender;

  const FeedWidget({
    Key? key,
    this.url = '',
    required this.fullName,
    required this.gender,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 4),
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.background,
        boxShadow: const [
          BoxShadow(
            offset: Offset(0, 2),
            spreadRadius: 2,
            blurRadius: 4,
            color: AppColors.gray30,
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppPhoto(
            height: 200,
            width: double.infinity,
            url: url,
          ),
          AppGenderLine(
            gender: gender,
            size: 8,
            axis: Axis.horizontal,
          ),
          Padding(
            padding: const EdgeInsets.all(12),
            child: Row(
              children: [
                Icon(
                  AppIcons.person,
                  color: Theme.of(context).colorScheme.secondary,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 6),
                  child: Text(
                    fullName,
                    style: TextStyle(
                      fontSize: 20,
                      color: Theme.of(context).colorScheme.secondary,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
