import 'package:flutter/material.dart';

const _lightValue = 0xFFFAF8F5;
const _darkValue = 0xFF504A55;

class AppColors {
  static const Color black = Color(0xFF111111);
  static const Color darkGray = Color(0xFF504A55);
  static const Color grey = Color(0xFFACACAC);
  static const Color milk = Color(0xFFFAF8F5);
  static const Color white = Color(0xFFFFFFFF);
  static const Color red = Color(0xFFEA2020);
  static const Color green = Color(0xFF458B79);
  static const Color pink = Color(0xfff19adb);
  static const Color blue = Color(0xff9ad0f1);
  static const Color yellow = Color(0xfff1ea9a);

  static const Color gray30 = Color(0x50ACACAC);

  const AppColors();
}

class AppMaterialColors {
  static const MaterialColor dark = MaterialColor(
    _darkValue,
    {
      50: AppColors.grey,
      100: AppColors.grey,
      200: AppColors.grey,
      300: AppColors.darkGray,
      400: AppColors.darkGray,
      500: Color(_darkValue),
      600: AppColors.darkGray,
      700: AppColors.black,
      800: AppColors.black,
      900: AppColors.black,
    },
  );
  static const MaterialColor light = MaterialColor(_lightValue, {
    50: AppColors.white,
    100: AppColors.white,
    200: AppColors.white,
    300: AppColors.milk,
    400: AppColors.milk,
    500: Color(_lightValue),
    600: AppColors.milk,
    700: AppColors.milk,
    800: AppColors.milk,
    900: AppColors.milk,
  });
}
